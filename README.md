Posthumanist Installation Version 1 "IterNoise": Interactive audio-visual sculpture

SCULPTURE:
The sculpture is an approach to play with a butterfly effect shown through the video feedback loop, which can be altered by voice/sound from the environment. In my research, I found an article with the title "The Butterfly Effect in Psychiatry", that explains on a case study how small events or hidden traumatic experiences can change the mental health of a person for the worse. Furthermore, I discovered reports about biases of psychologists themselves working with people diagnosed with a certain mental condition.

SOFTWARE:
While watching appearing patterns in the sculptures real-time animation created by a video loop feedback, there are forms and shapes we will recognize as faces, bodies, landscapes, or man-made constructions. The interpretation relies on each personal ability to build connections to the seen and its emotional related memories. The sound is generated by the mean of the summary of each pixels grayscale value every time a new video image has been created.
